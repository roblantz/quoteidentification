"""
    Novetta
    Christopher Sieber
    8/18/2017

    This is a python document that adds a single quote to list of stored quotes.
    Simply opens up file with stored quotes and adds to it.
"""


def addQuote(quote):
    with open('/home/csieber/flask/long_quotes.txt','a') as f:
        f.write(quote)
