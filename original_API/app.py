"""
    Novetta
    Christopher Sieber
    8/18/2017

    This is a python document that uses flask to run an API for
    quote extraction.
"""


from flask import Flask, render_template, request, flash, redirect, \
                    url_for, Markup, send_file
from wtforms import Form, StringField, TextAreaField, validators
from addQuote import addQuote
from classifyDocument import scoreDoc, fuzzyMatch
from datetime import datetime
from math import floor


app = Flask(__name__)
app.debug=True
app.secret_key = 'secretKey123' # need a secret key for sending files (logo)

colors = ['yellow', 'rgb(100,255,255)', 'rgb(100,255,0)', '#ffa0a0', \
          '#faa0fc']

links = []
for i in range(1,101):
    links.append('link' + str(i))


""" Home page """
@app.route('/')
def index():
    return render_template('home.html')


""" Class used to extract input text. Not exactly sure how TextAreaField works.
    A tutorial was followed that used this structure and it works.
"""
class QuoteForm(Form):
    body = TextAreaField('Body')


""" AddQuote page """
@app.route('/add_quote', methods=['GET','POST'])
def add_quote():
    form = QuoteForm(request.form)
    # Check if a post request was sent and check the form
    if request.method == 'POST' and form.validate():
        # First check if a file was uploaded
        if 'datafile' in request.files and \
                                   request.files['datafile'].filename != '':
            f = request.files['datafile']
            quotes = f.read()
            # Be sure that this is exactly one newline at the end of the text
            quotes = quotes.strip() + '\n'
        else: # Take data from text box
            quotes = form.body.data
            # Be sure that there is exactly one newline at the end of the text
            quotes = quotes.strip() + '\n'

        addQuote(quotes)

        """ formatting output """
        quotes = quotes.split('\n')
        added_quotes = []
        for i in range(0,3):
            if i < len(quotes):
                added_quotes.append(quotes[i])

        output = 'Quote/s added:<br>'
        for quote in added_quotes:
            output += quote + '<br>'

        if len(quotes) > 3:
            output += "...<br>"
            
        flash(Markup(output)) # post message to affirm quotes were added
        return redirect(url_for('add_quote'))        

    # Return html for page. If quotes were added flash() will display
    # message at top of web page
    return render_template('add_quote.html', form=form) 


""" Class used to extract input text. Not exactly sure how TextAreaField works.
    A tutorial was followed that used this structure and it works.
"""
class DocumentForm(Form):
    body = TextAreaField('Body')


""" This function takes in the matches and the original document and
    returns a string in html format that will highlight the matches.
"""
def highlightDocument(matches, doc):
    highlighted = ''

    # First split the document into paragraphs by newline
    doc_split_paragraphs = doc.split('\n')

    i=0
    for paragraph in doc_split_paragraphs:
        split = paragraph.split(". ") # split each paragraph into sentences
        for sentence in split:
            added = False   # boolean to determine if sentence was added
            for match, perc, fuzzy_match, fuzzy_perc, color in matches:
                # if fuzzy_match matches given sentence
                if fuzzy_match in sentence:
                    # add html highlighted sentence to string
                    highlighted += '<a style="color:black" name="' + \
                                   links[i] + '">' + \
                                   '<span style="background-color: ' + color + \
                                   '">' + sentence.strip() + ". " + \
                                   '</span></a>'
                    i += 1
                    added = True
            # if sentence was not a match, add sentence w/o highlight
            if not added and len(sentence) > 1:
                highlighted += sentence.strip() + '. '
        # Add newline to string for extra spacing     
        highlighted += '<br>'
                    
    return highlighted


""" Format matches for HTML string to be flashed """
def formatMatches(matches):
    output = ''
    for i in range(0,len(matches)):
        prob = str(int(floor(matches[i][1]*100))) + '%'
        fuzz_prob = str(matches[i][3]) + '%'
        for x in range(0, 6 - len(prob)):
            prob += '&ensp;'
        for x in range(0, 6 - len(fuzz_prob)):
            fuzz_prob += '&ensp;'

        output += "Matched quote:&emsp; &ensp;&ensp;" + prob + \
                  '&emsp;' + matches[i][0] + "<br>"
        output += 'Document quote:&emsp;&ensp;' + fuzz_prob + \
                  '&emsp;<a style="color:black" href="#' + links[i] + '">' + \
                  '<span style="background-color: ' + \
                  matches[i][4] + '">' + matches[i][2] + \
                  '</span></a><br><br><br>'

    output += '<pre style="font-family:arial;font-size:14px;">' + \
              '*** The probabilities for "Matched quote" are a ' + \
             'percentage greater than a threshold for the ' + \
             'percentages returned by crm114.<br>The threshold is set at ' + \
             '99.998, so even if a low percentage is shown, there is still' + \
             'a high chance of ' + \
             'the quote being a match. ***</pre><br>'
    
    return output
    
    
    
""" ScoreDocument page """
@app.route('/score_document', methods=['GET','POST'])
def score_document():
    # For timing purposes. Comment out if timing is not desired
    start = datetime.now()
    
    form = DocumentForm(request.form)
    # Check if a post request was sent and check the form
    if request.method == 'POST' and form.validate():
        # First check if a file was uploaded
        if 'datafile' in request.files and \
                                       request.files['datafile'].filename != '':
            f = request.files['datafile']
            body = f.read()
        else:
            body = form.body.data
            
        # match quotes to document
        matches = scoreDoc(body)
        # get actual quotes from document using fuzzy matching
        fuzzy_matches = fuzzyMatch(matches, body)


        """ assign background colors for each match """
        matches = []
        i=0
        for match, prob, fuzz, fuzz_prob in fuzzy_matches:
            matches.append((match, prob, fuzz, fuzz_prob, colors[i]))
            i += 1
            if i == len(colors):
                i = 0

    
        output = formatMatches(matches)

        # highlight matches and add them to output string
        highlighted_doc = highlightDocument(matches, body)
        output += "</br><h4>Document:</h4></br>" + highlighted_doc + \
                  '<br><br>'

        # post message of matches and document with highlighted matches
        flash(Markup(output))

        # For timing purposes. Comment out if timing is not desired
        print datetime.now() - start
        
        return redirect('/scored_document')        
    
    return render_template('score_document.html', form=form)


""" Page for a document that has just been scored """
@app.route('/scored_document')
def scoredDocument():
    return render_template('scored_document.html')


""" Novetta emblem image. Necessary for displaying emblem on navbar    
"""
@app.route('/novetta_mark_only_blue_RGB-small.png')
def novetta():
    return send_file("novetta_mark_only_blue_RGB-small.png")


if __name__ == '__main__':
    app.run(debug=True)
