Repository for the code used to create the original RESTful API. The classifyDocument code works in conjunction with app.py, but is older code with weaker performance when considering longer quotes and documents. See the 'crm114' directory for most up to date code for the program.

