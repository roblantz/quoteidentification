"""
    Novetta
    Christopher Sieber
    8/18/2017

    This is a python document that classifies quotes for a specific input
    document. Used in conjunction with 'app.py' to run API. It uses the
    CRM114 Discriminator to learn and classify documents. For more information
    on CRM114 visit crm114.sourceforg.net for more details.
"""


import os, time, csv, re, pycrm114 as p
from math import floor
from multiprocessing import Pool, Queue, Lock, Manager
from datetime import datetime
from functools import partial
from fuzzywuzzy import fuzz, process


def classify(num_quote, queue):    
    s = db.classify_text(quotes[num_quote])
    category = s.best_match()
    probability = s.tsprob()

    """ This is the threshold for the CRM114 classifier. The OSB microgroom
        returns a high percentage of match for many different matches so it's
        imperative to set a threshold to only allow the best matches through.
        Here you can change the threshold as you see fit. Original testing has
        it set at 99.998%. Anything higher will remain as a match and anything
        lower will be changed.
    """
    threshold = 99.998
    if probability*100 < threshold and category == 'quote':
        category = 'nonquote*'

    probability = (probability*100 - threshold) / (100 - threshold)

    # Print statement that shows how fast you are going through the dataset
    if num_quote % 100000 == 0:
        print(num_quote)

    if category == 'quote':
        queue.put((quotes[num_quote],probability))
        # print statements show all matches
        print(probability*100)
        print(quotes[num_quote])



def scoreDoc(doc_text):
    global document     # create global variable with document text
    document = doc_text 

    """ Create classifier using OSB microgroom and train on document. OSB was
        found to be fastest and most accurate classifier available.
    """

    cb = p.ControlBlock(flags=(p.CRM114_OSB | p.CRM114_MICROGROOM | \
    #cb = p.ControlBlock(flags=(p.CRM114_DEFAULT | \
                               p.CRM114_STRING),
                        classes=[("quote", True), ("nonquote", False)],
                        start_mem = 8000000)
    global db   # create global variable so it can be used in classify()
    db = p.DataBlock(cb)

    # train on document as whole and on smaller sections for better accuracy
    parts = document.split('\n')
    db.learn_text(0, document)
    for line in parts:
        db.learn_text(0, line)
        db.learn_text(0, line.lower())
        db.learn_text(0, line.upper())
        db.learn_text(0, line.title())
    

    """ Classify all quotes by starting a pool of processes that runs different
        instances of classify(). This multiprocessing is essential to the speed
        of this program, DO NOT REMOVE. Each instance is given a global queue to
        add classification to. 
    """
    # read in list of all quotes to global variable to be used in classify()
    global quotes
    with open('/home/csieber/flask/long_quotes.txt','r') as f:
        quotes = f.readlines()

    # setup, start, and end pool of instances used to classify
    manager = Manager()
    queue = manager.Queue()
    partial_classify = partial(classify, queue=queue)
    pool = Pool()
    pool.map(partial_classify, range(0,len(quotes)))
    pool.close()
    pool.join()

    # extract classifications from queue and add them to list
    # STOP is added to queue to signal an end to the stream of classifications
    matches = []
    queue.put('STOP')
    for i in iter(queue.get, 'STOP'):
        matches.append(i)

    # print statement for total matches. Uncomment if unwanted
    print("Matches\t\tTotal Matches: %d\n" % len(matches))

    return matches


""" Function for fuzzy matching. Takes in a list of matches and a document and
    finds the best document sentence that matches the given match. Accurate for
    close matches from CRM114. If the given match isn't close to any sentence
    there is unexpected behavior. This is why CRM114 is used to classify; it's
    much more accurate than the fuzzy matcher. This function is just used for
    extracting the best quote from the document for each match. Sometimes
    matches from CRM114 end up mapping to the same sentence. If so, only the
    best match is used. This eliminates any redundancy.
"""
def fuzzyMatch(matches, doc):
    # First split document into sentences to be matched with CRM114 matches
    doc_split_paragraphs = doc.split('\n')
    doc_split_sentences = []
    for paragraph in doc_split_paragraphs:
        doc_split_sentences.extend(paragraph.split(". "))

    # Finds all fuzzy matches for each CRM114 match and adds to list
    fuzzy_matches = []
    for match, prob in matches:
        match_tuple = process.extractOne(match, doc_split_sentences)
        # If no fuzzy match is found, disregard match
        if match_tuple:
            fuzzy_matches.append((match, prob, match_tuple[0], match_tuple[1]))
            

    """ This chunk of code eliminates redundancies within matches. If there are
        any redundant matches it finds the best fuzzy match for each CRM114
        match and only adds that one.
    """
    final_matches = []
    # loop through all sentences in document, find best matches
    for sentence in doc_split_sentences:
        # initialize sentence to no fuzzy match
        final_match = ''
        final_prob = 0
        final_fuzzy = ''
        final_fuzzy_prob = 0
        # loop through all matches looking for current sentence
        for match, prob, fuzzy, fuzzy_prob in fuzzy_matches:
            # if fuzzy match is better than previous match
            if fuzzy == sentence and fuzzy_prob >= final_fuzzy_prob:
                final_match = match
                final_prob = prob
                final_fuzzy = fuzzy
                final_fuzzy_prob = fuzzy_prob
                
        # if any fuzzy matches were found for current sentence, add best match
        print("fuzzy prob: %d" % final_fuzzy_prob)
        if final_fuzzy_prob > 86:
            final_matches.append((final_match, final_prob,
                                 final_fuzzy, final_fuzzy_prob))
                
    
    return final_matches













    
                          
    
        
