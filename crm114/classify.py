"""
    Novetta
    Christopher Sieber
    8/18/2017

    This is a python document that classifies quotes for a specific input
    document. Used in conjunction with 'app.py' to run API. It uses the
    CRM114 Discriminator to learn and classify documents. For more information
    on CRM114 visit crm114.sourceforg.net for more details.
"""


import os, time, csv, re, pycrm114 as p
from math import floor
from multiprocessing import Pool, Queue, Lock, Manager
from datetime import datetime
from functools import partial
from fuzzywuzzy import fuzz, process

with open('/home/csieber/flask/spanish_docs/quote%d.txt' % 2,'r') as f:
    quote1 = f.read()

def classify(num_quote, queue):    
    s = db.classify_text(quotes[num_quote])
    category = s.best_match()
    probability = s.tsprob()
    pr = s.overall_pR()


    """ This is the threshold for the CRM114 classifier. The OSB microgroom
        returns a high percentage of match for many different matches so it's
        imperative to set a threshold to only allow the best matches through.
        Here you can change the threshold as you see fit. Original testing has
        it set at 99.99995%. Anything higher will remain as a match and anything
        lower will be changed and discarded.
    """
    
    threshold = 99.99995
    if probability*100 < threshold and category == 'quote':
        category = 'nonquote*'


    # Print statement that shows how fast you are going through the dataset
    if num_quote % 100000 == 0:
        print(num_quote)

    if category == 'quote':
        queue.put((quotes[num_quote],pr))
        # print statements show all matches
        print(probability*100)
        print(pr)
        print(quotes[num_quote].rstrip())
        print("num_quote: %d" % num_quote)
        print ("Best match: %s  Tot succ prob: %f  overall_pR: %f  unk_features: %d"
           % (s.best_match(), s.tsprob(), s.overall_pR(), s.unk_features()))
        for sc in s.scores():
            print ("documents: %d  features: %d  hits: %d  prob: %f  pR: %f\n" %
                   (sc["documents"], sc["features"], sc["hits"], sc["prob"], sc["pR"]))
        


def scoreDoc(doc_text):
    global document     # create global variable with document text
    document = doc_text 

    """ Create classifier using OSB microgroom and train on document. OSB was
        found to be fastest and most accurate classifier available.
    """

    cb = p.ControlBlock(flags=(p.CRM114_OSB_BAYES | p.CRM114_MICROGROOM | \
                               p.CRM114_STRING),
                        classes=[("quote", True), ("nonquote", False)],
                        start_mem = 8000000)
    global db   # create global variable so it can be used in classify()
    db = p.DataBlock(cb)

    # train on document as whole and on smaller sections for better accuracy
    parts = document.split('\n')
    db.learn_text(0, document)
    """for line1 in parts:
        sentences = line1.split('. ')
        for line in sentences:
            db.learn_text(0, line)
            db.learn_text(0, line.lower())
            db.learn_text(0, line.upper())
            db.learn_text(0, line.title())
    """

    """ Classify all quotes by starting a pool of processes that runs different
        instances of classify(). This multiprocessing is essential to the speed
        of this program, DO NOT REMOVE. Each instance is given a global queue to
        add classification to. 
    """
    # read in list of all quotes to global variable to be used in classify()
    global quotes
    with open('/home/csieber/flask/gni/quotes/quotes.txt','r') as f:
        quotes = f.read().splitlines()
        

    # setup, start, and end pool of instances used to classify
    manager = Manager()
    queue = manager.Queue()
    partial_classify = partial(classify, queue=queue)
    pool = Pool()
    pool.map(partial_classify, range(0,len(quotes)))
    pool.close()
    pool.join()

    # extract classifications from queue and add them to list
    # STOP is added to queue to signal an end to the stream of classifications
    matches = []
    queue.put('STOP')
    for i in iter(queue.get, 'STOP'):
        matches.append(i)

    # print statement for total matches. Uncomment if unwanted
    print("Matches\t\tTotal Matches: %d\n" % len(matches))

    return matches



#timing
start = datetime.now()

num = '880000096191';
with open('/home/csieber/flask/gni/articles/%s' % num,'r') as f:
    doc = f.read()
    
matches = scoreDoc(doc)

print datetime.now() - start
        
